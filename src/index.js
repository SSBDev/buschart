import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div className="parent">
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <App />
    <div id="div23"> Lane 3 </div>
    <div id="div24"> Lane 2 </div>
    <div id="div25"> Lane 1</div>
    <div id="div26"> BUILDING 4 </div>
    <div id="div27"> BUILDING 3 </div>
    <div id="div28"> BUILDING 2 </div>
    <div id="div29"> PARKING </div>
  </div>
);
