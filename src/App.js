
export default function App() {
  if (typeof App.counter === "undefined") {
    App.counter = 0;
  }
  App.counter++;
  
  const towns = ['Bus Options', 'Booton', 'Bloom', 'Butler', 'Chat', 'Dov', 'HP', 'J-Hop', 'J-OR', 'JTI', 'LP', 'Madison', 'Mend', 'Mont 1', 'Mont 2', 'Mont 3', 'Morris', 'Mt. O', 'MH (Dorene)', 'MH (Carlos)', 'MH (Lilian)', 'MH (Maria)', 'P101', 'P102', 'P103', 'P104', 'P105', 'PEQ', 'Rand', 'Rox', 'RK', 'WM']; 
  const townList = towns.map((town) => <option key={town}>{town}</option> );
  return (
    <div id={"div" + App.counter} className="target">
      <select>
        {townList}
      </select>
    </div>
  );
}
